This is the repository of Framasoft's DockerFiles.

The docker images are published under the `framasoft` account on the [Docker store](https://store.docker.com/profiles/framasoft).

* [framasite-php-7.0](https://store.docker.com/community/images/zanata/framasite-php-7.0) provides a test environment for [Framasite](https://framagit.org/framasoft/framasite)
* [framasite-php-7.1](https://store.docker.com/community/images/zanata/framasite-php-7.1) provides a test environment for [Framasite](https://framagit.org/framasoft/framasite)
* [zanata](https://store.docker.com/community/images/framasoft/zanata) provides a slightly modified docker image of [Zanata server](https://store.docker.com/community/images/zanata/server)
