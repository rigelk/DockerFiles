framadate-ci:
	make -C framadate/

framasite:
	make -C framasite/

push-trad:
	make -C push-trad/

zanata:
	make -C zanata/

epub:
	make -C epub/
